jQuery(document).ready(function() {
    jQuery('.animete_class_inleft').addClass("hidden_box").viewportChecker({
        classToAdd: 'visible_box animated bounceInLeft', // Class to add to the elements when they are visible
        offset: 100
    });
    jQuery('.animete_class_zoomIn').addClass("hidden_box").viewportChecker({
        classToAdd: 'visible_box animated zoomIn', // Class to add to the elements when they are visible
        offset: 100
    });
    jQuery('.animete_class_inDown').addClass("hidden_box").viewportChecker({
        classToAdd: 'visible_box animated bounceInDown', // Class to add to the elements when they are visible
        offset: 100
    });
    jQuery('.animete_class_inUp').addClass("hidden_box").viewportChecker({
        classToAdd: 'visible_box animated bounceInUp', // Class to add to the elements when they are visible
        offset: 100
    });
    jQuery('.animete_class_inUp2').addClass("hidden_box").viewportChecker({
        classToAdd: 'visible_box_bootom animated bounceInUp', // Class to add to the elements when they are visible
        offset: 100
    });
    jQuery('.animete_class_inRight').addClass("hidden_box").viewportChecker({
        classToAdd: 'visible_box animated bounceInRight', // Class to add to the elements when they are visible
        offset: 100
    });
    jQuery('.animete_class_rotateIn').addClass("hidden_box").viewportChecker({
        classToAdd: 'visible_box animated rotateIn', // Class to add to the elements when they are visible
        offset: 100
    });
});