<?php

class Validated {

    protected $mailError = [];


    public function phoneValidate($phone) {

        if (!preg_match('/^(\+|\d){1}\d{6,12}$/', $phone)) {
            $this->mailError['phone'] = 'Вы не правильно ввели телефон, пример: 0957849875';
            return false;
        }

        return true;

    }

    public function mailValidate($mail) {
        if (!preg_match('/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/', $mail)) {
            $this->mailError['mail'] = 'Вы не правильно ввели email, пример: eximple@gmail.com';
            return false;
        }
        return true;
    }

    public function messageValidate($form_message) {
        if (!(strlen($form_message) > 5 && strlen($form_message) < 500)) {
            $this->mailError['form_message'] = 'Длина сообщения должна быть не меньше 5 символов и не больше 500';
            return false;
        }
        return true;
    }

    public function getError() {
       return $this->mailError;
    }
}